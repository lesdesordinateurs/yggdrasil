# Copyright (C) 2019  Les désordinateurs communicants

# Author: Mathieu Quentel <contact@mathieu-quentel.ovh>
#         Damien Cassou <damien@cassou.me>
# Url: https://framagit.org/lesdesordinateurs/yggdrasil
# Version: 0.1.0

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

all: test

test:
	PYTHONPATH="src" python3 test/testHold.py
	PYTHONPATH="src" python3 test/testMovement.py
	PYTHONPATH="src" python3 test/testStep.py
	PYTHONPATH="src" python3 test/testBodyPart.py
	PYTHONPATH="src" python3 test/testRouteBuilder.py

run-demo:
	python3 src/main.py \
		--left-hand test/data/route1/left_hand.log \
		--right-hand test/data/route1/right_hand.log \
		--left-foot test/data/route1/left_foot.log \
		--right-foot test/data/route1/right_foot.log \
		pretty

.PHONY: all test run-demo
