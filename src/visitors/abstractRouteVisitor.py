# -*- coding: utf-8 -*-
# Copyright (C) 2019  Les désordinateurs communicants

# Author: Mathieu Quentel <contact@mathieu-quentel.ovh>
#         Damien Cassou <damien@cassou.me>
# Url: https://framagit.org/lesdesordinateurs/yggdrasil
# Version: 0.1.0

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

class AbstractRouteVisitor(object):
    def visitAll(self, entities):
        for entity in entities:
            entity.accept(self)

    def visitRoute(self, route):
        self.visitAll(route.getSteps())

    def visitStep(self, step):
        self.visitAll(step.getAssignedHolds())

    def visitAssignedHold(self, assignedHold):
        self.visitBodyPart(assignedHold.getBodyPart())
        self.visitHold(assignedHold.getHold())

    def visitBodyPart(self, bodyPart):
        pass

    def visitHold(self, hold):
        self.visitAll(hold.getNeighbors())

    def visitNeighbor(self, neighbor):
        self.visitHold(neighbor.getHold())

    def visitMovement(self, movement):
        self.visitBodyPart(movement.getBodyPart())
