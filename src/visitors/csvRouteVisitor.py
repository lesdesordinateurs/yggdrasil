# -*- coding: utf-8 -*-
# Copyright (C) 2019  Les désordinateurs communicants

# Author: Mathieu Quentel <contact@mathieu-quentel.ovh>
#         Damien Cassou <damien@cassou.me>
# Url: https://framagit.org/lesdesordinateurs/yggdrasil
# Version: 0.1.0

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from visitors.abstractRouteVisitor import AbstractRouteVisitor

# Export a route into a bunch of CSV files
class CsvRouteVisitor(AbstractRouteVisitor):
    def __init__(self, connection):
        pass

    def visitRoute(self, route):
        pass

    def visitStep(self, step):
        pass

    def visitAssignedHold(self, assignedHold):
        pass

    def visitBodyPart(self, bodyPart):
        pass

    def visitHold(self, hold):
        pass

    def visitNeighbor(self, neighbor):
        pass

    def visitMovement(self, movement):
        pass

    @classmethod
    def name(cls):
        return 'csv'
