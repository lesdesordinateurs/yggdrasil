# -*- coding: utf-8 -*-
# Copyright (C) 2019  Les désordinateurs communicants

# Author: Mathieu Quentel <contact@mathieu-quentel.ovh>
#         Damien Cassou <damien@cassou.me>
# Url: https://framagit.org/lesdesordinateurs/yggdrasil
# Version: 0.1.0

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from visitors.abstractRouteVisitor import AbstractRouteVisitor

class SqliteRouteVisitor(AbstractRouteVisitor):
    def __init__(self, connection):
        self.connection = connection
        self.cursor = connection.cursor()

    def visitRoute(self, route):
        pass

    def visitStep(self, step):
        pass

    def visitAssignedHold(self, assignedHold):
        pass

    def visitBodyPart(self, bodyPart):
        pass

    def visitHold(self, hold):
        pass

    def visitNeighbor(self, neighbor):
        pass

    def visitMovement(self, movement):
        pass

    def _create_tables(self):
        self._createRoutesTable()
        self._createStepsTable()

    def _createRoutesTable(self):
        self.cursor.execute("""
        CREATE TABLE IF NOT EXISTS routes (
          id INTEGER NOT NULL PRIMARY KEY,
          name TEXT NOT NULL
        );
        """)

    def _createStepsTable(self):
        self.cursor.execute("""
        CREATE TABLE IF NOT EXISTS steps (
          id INTEGER NOT NULL PRIMARY KEY,
          route_id INTEGER NOT NULL,
          FOREIGN KEY (route_id) REFERENCES routes (id)
        );
        """)

    @classmethod
    def name(cls):
        return 'sqlite'
