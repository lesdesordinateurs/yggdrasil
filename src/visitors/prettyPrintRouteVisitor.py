# -*- coding: utf-8 -*-
# Copyright (C) 2019  Les désordinateurs communicants

# Author: Mathieu Quentel <contact@mathieu-quentel.ovh>
#         Damien Cassou <damien@cassou.me>
# Url: https://framagit.org/lesdesordinateurs/yggdrasil
# Version: 0.1.0

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from visitors.abstractRouteVisitor import AbstractRouteVisitor

# Write a route to standard output in a human readable way.
class PrettyPrintRouteVisitor(AbstractRouteVisitor):
    def __init__(self):
        self.currentStep = None

    def visitRoute(self, route):
        self.currentStep = route.getFirstStep()
        super().visitRoute(route)

    def visitStep(self, step):
        movements = self.currentStep.movementTo(step)
        self.visitAll(movements)
        self.currentStep = step

    def visitMovement(self, movement):
        print(movement)

    @classmethod
    def name(cls):
        return 'pretty'
