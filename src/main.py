# -*- coding: utf-8 -*-
# Copyright (C) 2019  Les désordinateurs communicants

# Author: Mathieu Quentel <contact@mathieu-quentel.ovh>
#         Damien Cassou <damien@cassou.me>
# Url: https://framagit.org/lesdesordinateurs/yggdrasil
# Version: 0.1.0

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from model.movement import Movement

from routeBuilder import RouteBuilder
from argumentParser import ArgumentParser

# Return a Route instance by reading the bodyPartLogFiles. Each
# element of `bodyPartLogFiles` is a pair (bodyPart, logFile) that
# associates a body part to a log file.
def createRoute(bodyPartLogFiles):
    bodyPartEvents = []

    for (bodyPart, logFile) in bodyPartLogFiles:
        for line in logFile:
            fields = line[:-1].split(',') # remove the \n
            timestamp=fields[0]
            # everything after timestamp is considered the position
            position=fields[1:]

            bodyPartEvents.append((timestamp, bodyPart, position))

    list.sort(bodyPartEvents, key=(lambda bodyPartEvent: bodyPartEvent[0]))

    routeBuilder = RouteBuilder()

    for (timestamp, bodyPart, position) in bodyPartEvents:
        routeBuilder.addMovement(Movement(bodyPart, position))

    return routeBuilder.build()

if __name__ == '__main__':
    argumentParser = ArgumentParser()
    argumentParser.parseArguments()

    logFiles = argumentParser.getLogFiles()
    route = createRoute(logFiles)
    exporter = argumentParser.getExporter()

    route.accept(exporter)
