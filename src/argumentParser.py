# -*- coding: utf-8 -*-
# Copyright (C) 2019  Les désordinateurs communicants

# Author: Mathieu Quentel <contact@mathieu-quentel.ovh>
#         Damien Cassou <damien@cassou.me>
# Url: https://framagit.org/lesdesordinateurs/yggdrasil
# Version: 0.1.0

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse

from model.bodyPart import BodyPart

from visitors.routeVisitorFactory import RouteVisitorFactory

# This class is responsible for creating a command-line argument
# parser. It makes it possible for the user to pass log files for each
# body part and to choose the exporter.
class ArgumentParser(object):
    def __init__(self):
        self.parser = argparse.ArgumentParser()

        self._configureLogFilesArguments()
        self._configureExporterArguments()

    # Read the arguments passed by the user on the command line.
    def parseArguments(self):
        self.args = vars(self.parser.parse_args())

    # Return a list of paires (bodyPart, logFile) for each log file
    # passed as argument.
    def getLogFiles(self):
        logFiles = []

        for bodyPart in BodyPart.list():
            bodyPartName = bodyPart.getOption().replace('-','_')
            logFile = self.args[bodyPartName]

            if logFile != None:
                logFiles.append((bodyPart, logFile))

        return logFiles

    # Return an exporter based on the user choice.
    def getExporter(self):
        exporterName = self.args['exporter']
        return RouteVisitorFactory().createExporterNamed(exporterName)

    def _configureLogFilesArguments(self):
        logFilesGroup = self.parser.add_argument_group(
            'Add a log file per body part'
        )

        for bodyPart in BodyPart.list():
            logFilesGroup.add_argument(
                '--' + bodyPart.getOption(),
                type=argparse.FileType('r'),
                metavar='FILE',
                help="Filename of the log for the {partName}".format(
                    partName=bodyPart.getName()
                ))

    def _configureExporterArguments(self):
        exporterGroup = self.parser.add_argument_group(
            'Export the result'
        )

        nameToExporterClass = RouteVisitorFactory().nameToExporterClass()
        names = list(iter(nameToExporterClass))

        exporterGroup.add_argument(
            'exporter',
            choices=names,
            metavar='EXPORTER',
            help="the exporter to use ({names})".format(
                names = ', '.join(names)
            )
        )
