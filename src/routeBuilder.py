# -*- coding: utf-8 -*-
# Copyright (C) 2019  Les désordinateurs communicants

# Author: Mathieu Quentel <contact@mathieu-quentel.ovh>
#         Damien Cassou <damien@cassou.me>
# Url: https://framagit.org/lesdesordinateurs/yggdrasil
# Version: 0.1.0

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from model.route import Route
from model.step import Step
from model.assignedHold import AssignedHold
from model.hold import Hold
from model.neighbor import Neighbor
from model.bodyPart import BodyPart

# A route builder creates a climber's route by reading an ordered list
# of movements.
class RouteBuilder(object):
    def __init__(self):
        self.steps = []

        initialStep = self._createInitialStep()
        self.steps.append(initialStep)

    # Add the movement parameter to the route being built. Each
    # movement is from the current step to a new one.
    def addMovement(self, movement):
        lastStep = self.steps[-1]
        newStep = lastStep.copyWithMovement(movement)
        self.steps.append(newStep)

    # Return a route consisting of the steps that got created by
    # calling `addMovement()` repeatedly.
    def build(self):
        route = Route(self.steps)
        return route

    # Create the first step, where each body part is at their
    # respective (0,0,0) coordinate.
    def _createInitialStep(self):
        initialAssignedHolds = []

        for bodyPart in BodyPart.list():
            hold = Hold()
            assignedHold = AssignedHold(bodyPart, hold)
            initialAssignedHolds.append(assignedHold)

        return Step(initialAssignedHolds)
