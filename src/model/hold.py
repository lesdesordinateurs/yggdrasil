# -*- coding: utf-8 -*-
# Copyright (C) 2019  Les désordinateurs communicants

# Author: Mathieu Quentel <contact@mathieu-quentel.ovh>
#         Damien Cassou <damien@cassou.me>
# Url: https://framagit.org/lesdesordinateurs/yggdrasil
# Version: 0.1.0

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# This represents a location on a climber's route where the climber
# can put a hand or foot. Each hold is associated to a list of
# neighbors, each one representing another hold with a position
# relative to the receiver.
class Hold(object):
  def __init__(self, neighbors = []):
    self.neighbors = neighbors

  def accept(self, visitor):
    visitor.visitHold(self)

  # Return the neighbor pointing to `hold`
  def getNeighbor(self, hold):
    return next(filter(
      lambda neighbor: neighbor.getHold() == hold,
      self.neighbors
    ))

  def getNeighbors(self):
    return self.neighbors

  def addNeighbor(self, neighbor):
    self.neighbors.append(neighbor)
