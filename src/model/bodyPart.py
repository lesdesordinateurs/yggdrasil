# -*- coding: utf-8 -*-
# Copyright (C) 2019  Les désordinateurs communicants

# Author: Mathieu Quentel <contact@mathieu-quentel.ovh>
#         Damien Cassou <damien@cassou.me>
# Url: https://framagit.org/lesdesordinateurs/yggdrasil
# Version: 0.1.0

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from enum import Enum

# This represents a part of human body where a sensor has been
# attached.
class BodyPart(Enum):
    LEFT_HAND = {
        'name':'left hand',
        'option':'left-hand'
    }
    RIGHT_HAND = {
        'name':'right hand',
        'option':'right-hand'
    }
    LEFT_FOOT = {
        'name':'left foot',
        'option':'left-foot'
    }
    RIGHT_FOOT = {
        'name':'right foot',
        'option':'right-foot'
    }

    def accept(self, visitor):
        visitor.visitBodyPart(self)

    def getName(self):
        return self.value['name']

    def getOption(self):
        return self.value['option']

    def __str__(self):
        return self.getName()

    @classmethod
    def list(cls):
        return list(cls)

if __name__ == '__main__':
    print(BodyPart.list())
