# -*- coding: utf-8 -*-
# Copyright (C) 2019  Les désordinateurs communicants

# Author: Mathieu Quentel <contact@mathieu-quentel.ovh>
#         Damien Cassou <damien@cassou.me>
# Url: https://framagit.org/lesdesordinateurs/yggdrasil
# Version: 0.1.0

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from model.movement import Movement
from model.assignedHold import AssignedHold
from model.hold import Hold
from model.neighbor import Neighbor

# A step is a set of holds assigned to human parts. In other words, it
# is a position of a climber at a moment in time when s·he is stable
# with each body part on a hold.
class Step(object):
    def __init__(self, assignedHolds):
      self.assignedHolds = set(assignedHolds)

    def accept(self, visitor):
        visitor.visitStep(self)

    def getAssignedHolds(self):
        return self.assignedHolds

    # Return the hold that bodyPart is currently attached to.
    def getHold(self, bodyPart):
        assignedHold = next(filter(
            lambda assignedHold: assignedHold.getBodyPart() == bodyPart,
            self.assignedHolds
        ))

        return assignedHold.getHold()

    # Return required movements to go from the receiver to nextStep.
    def movementTo(self, nextStep):
        nextAssignedHolds = nextStep.getAssignedHolds()
        changes = nextAssignedHolds.difference(self.assignedHolds)
        movements = []

        for change in changes:
            changedBodyPart = change.getBodyPart()
            newHold = change.getHold()
            currentHold = self.getHold(changedBodyPart)
            neighbor = currentHold.getNeighbor(newHold)
            movement = Movement(changedBodyPart, neighbor.getPosition())

            movements.append(movement)

        return movements

    # Create and return a new step which is similar to the receiver
    # except it is after `movement`. Basically, all assigned holds of
    # the receiver are copied as is except for the one corresponding
    # to `movement`.
    def copyWithMovement(self, movement):
        bodyPart = movement.getBodyPart()
        position = movement.getPosition()

        previousHoldForBodyPart = self.getHold(bodyPart)
        newHold = Hold()
        neighbor = Neighbor(position, newHold)
        previousHoldForBodyPart.addNeighbor(neighbor)

        newAssignedHolds = []

        for assignedHold in self.assignedHolds:
            newAssignedHold = None

            if assignedHold.getBodyPart() == bodyPart:
                newAssignedHold = AssignedHold(bodyPart, newHold)
            else:
                newAssignedHold = assignedHold

            newAssignedHolds.append(newAssignedHold)

        return Step(newAssignedHolds)
