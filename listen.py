# Code from: https://github.com/xioTechnologies/NGIMU-Python-Example

# NGIMU UDP Demo Python 3 script Requires python-osc 1.7.0
# https://pypi.org/project/python-osc/

import argparse
import math
import csv
import time

outputFile =""
dataLine = "/sensors"
linearDataLine = "/linear"

from pythonosc import dispatcher
from pythonosc import osc_server

def handleRawSensorData(lineStart, gyroX, gyroY, gyroZ, accX, accY, accZ, magX, magY, magZ, pressure):
	outputWriter.writerow([str(time.time()), gyroX, gyroY, gyroZ, accX, accY, accZ, magX, magY, magZ, pressure])

def handleLinearAccelerationData(lineStart, accX, accY, accZ):
	linearOutputWriter.writerow([str(time.time()), accX, accY, accZ])

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("--ip", default="0.0.0.0", help="The ip to listen on")
  parser.add_argument("--port",
      type=int, default=8000, help="The port to listen on")
  parser.add_argument("--output", default="IMU_log.csv", help="The CSV output file")
  args = parser.parse_args()
  outputFile = args.output
  outputCSVfile = open(outputFile, 'w', newline='')
  outputLinearCSVfile = open("linear_" + outputFile, 'w', newline='')
  outputWriter = csv.writer(outputCSVfile, delimiter=';',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
  outputWriter.writerow(['Timestamp', 'GyroX', 'GyroY', 'GyroZ', 'AccX', 'AvvY', 'AccZ', 'MagX', 'MagY', 'MagZ', 'Pressure'])
  linearOutputWriter = csv.writer(outputLinearCSVfile, delimiter=';',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
  linearOutputWriter.writerow(['Timestamp', 'AccX', 'AccY', 'AccZ'])

  dispatcher = dispatcher.Dispatcher()
  dispatcher.map("/sensors", handleRawSensorData)
  dispatcher.map("/linear", handleLinearAccelerationData)
#  dispatcher.map("/quaternion", print)
#  dispatcher.map("/battery", print)
#  dispatcher.map("/", print)

  server = osc_server.ThreadingOSCUDPServer(
      (args.ip, args.port), dispatcher)
  print("Serving on {}".format(server.server_address))
  server.serve_forever()
