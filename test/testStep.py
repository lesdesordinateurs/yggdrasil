# -*- coding: utf-8 -*-
# Copyright (C) 2019  Les désordinateurs communicants

# Author: Mathieu Quentel <contact@mathieu-quentel.ovh>
#         Damien Cassou <damien@cassou.me>
# Url: https://framagit.org/lesdesordinateurs/yggdrasil
# Version: 0.1.0

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import unittest

from model.assignedHold import AssignedHold
from model.bodyPart import BodyPart
from model.hold import Hold
from model.neighbor import Neighbor
from model.step import Step
from model.movement import Movement

class TestStep(unittest.TestCase):
    def test_getHold(self):
        holdC = Hold([])
        neighborCOfA = Neighbor([0,1,0], holdC)
        holdA = Hold([neighborCOfA])
        holdB = Hold([])

        assignedHoldLH_A = AssignedHold(BodyPart.LEFT_HAND, holdA)
        assignedHoldRH_B = AssignedHold(BodyPart.RIGHT_HAND, holdB)
        assignedHoldLH_C = AssignedHold(BodyPart.LEFT_HAND, holdC)

        step1 = Step([assignedHoldLH_A, assignedHoldRH_B])

        resultLH = step1.getHold(BodyPart.LEFT_HAND)
        resultRH = step1.getHold(BodyPart.RIGHT_HAND)

        self.assertEqual(resultLH, holdA)
        self.assertEqual(resultRH, holdB)

    def test_movementTo(self):
        holdC = Hold([])
        neighborCOfA = Neighbor([0,1,0], holdC)
        holdA = Hold([neighborCOfA])
        holdB = Hold([])

        assignedHoldLH_A = AssignedHold(BodyPart.LEFT_HAND, holdA)
        assignedHoldRH_B = AssignedHold(BodyPart.RIGHT_HAND, holdB)
        assignedHoldLH_C = AssignedHold(BodyPart.LEFT_HAND, holdC)

        step1 = Step([assignedHoldLH_A, assignedHoldRH_B])
        step2 = Step([assignedHoldLH_C, assignedHoldRH_B])

        movements = step1.movementTo(step2)
        movement = movements[0]

        self.assertEqual(len(movements), 1)
        self.assertEqual(movement.getBodyPart(), BodyPart.LEFT_HAND)
        self.assertEqual(movement.getPosition(), [0,1,0])

    def test_copyWithMovement(self):
        holdA = Hold([])
        holdB = Hold([])

        assignedHoldLH_A = AssignedHold(BodyPart.LEFT_HAND, holdA)
        assignedHoldRH_B = AssignedHold(BodyPart.RIGHT_HAND, holdB)

        initialStep = Step([assignedHoldLH_A, assignedHoldRH_B])
        movement = Movement(BodyPart.LEFT_HAND, [0,1,0])

        result = initialStep.copyWithMovement(movement)
        lhHold = result.getHold(BodyPart.LEFT_HAND)
        rhHold = result.getHold(BodyPart.RIGHT_HAND)

        self.assertEqual(len(result.getAssignedHolds()), 2)

        # The left hand moved:
        self.assertEqual(len(holdA.getNeighbors()), 1)
        self.assertEqual(holdA.getNeighbor(lhHold).getPosition(), [0,1,0])
        # The right hand didn't move:
        self.assertEqual(len(holdB.getNeighbors()), 0)
        self.assertEqual(holdB, rhHold)

if __name__ == '__main__':
    unittest.main()
