# -*- coding: utf-8 -*-
# Copyright (C) 2019  Les désordinateurs communicants

# Author: Mathieu Quentel <contact@mathieu-quentel.ovh>
#         Damien Cassou <damien@cassou.me>
# Url: https://framagit.org/lesdesordinateurs/yggdrasil
# Version: 0.1.0

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import unittest

from model.movement import Movement
from model.bodyPart import BodyPart

from routeBuilder import RouteBuilder

class TestRouteBuilder(unittest.TestCase):
    def test_simpleRoute(self):
        routeBuilder = RouteBuilder()

        routeBuilder.addMovement(Movement(BodyPart.LEFT_HAND, [0,1,0]))
        routeBuilder.addMovement(Movement(BodyPart.RIGHT_HAND, [0,1,0]))
        routeBuilder.addMovement(Movement(BodyPart.LEFT_HAND, [1,1,0]))
        routeBuilder.addMovement(Movement(BodyPart.RIGHT_HAND, [-1,1,0]))

        route = routeBuilder.build()
        steps = route.getSteps()

        self.assertEqual(len(steps), 5)

if __name__ == '__main__':
    unittest.main()
