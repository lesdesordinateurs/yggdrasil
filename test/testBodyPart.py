# -*- coding: utf-8 -*-
# Copyright (C) 2019  Les désordinateurs communicants

# Author: Mathieu Quentel <contact@mathieu-quentel.ovh>
#         Damien Cassou <damien@cassou.me>
# Url: https://framagit.org/lesdesordinateurs/yggdrasil
# Version: 0.1.0

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import unittest
from model.bodyPart import BodyPart
from model.hold import Hold
from model.neighbor import Neighbor
from model.movement import Movement

class TestBodyPart(unittest.TestCase):
    # def test_str(self):
    #     result = str(BodyPart.RIGHT_FOOT)

    #     self.assertEqual(result, "right foot")

    def test_eq(self):
        bodyPart = BodyPart.list()[0]

        bodyParts = [
            BodyPart.LEFT_HAND,
            BodyPart.RIGHT_HAND,
            BodyPart.LEFT_FOOT,
            BodyPart.RIGHT_FOOT,
        ]

        self.assertTrue(bodyPart in bodyParts)

if __name__ == '__main__':
    unittest.main()
