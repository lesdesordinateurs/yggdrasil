# Yggdrasil

Note to Python developers: I don't know Python's idioms so your eyes
might bleed a bit while reading the source code.

## Testing

Run the suite of tests with:

```session
$ make test
```

## Launching the application

You can run the application with:

```session
$ python3 src/main.py --help
```

You need to pass at least one log file for a body part and an
exporter.

To run the demo with fake data, type:

```session
$ make run-demo
```

## Todo

Before this application can be used, a real exporter has to be
finished. The CSV and Sqlite exporters have dedicated files that needs
to be filled in.

## Extension points

To extend (i.e., "finish" is closer to the truth) the application, you
can add visitors (i.e., exporters) and body parts.

### Visitors

The visitors in `src/visitors` follow the Visitor design pattern. The
CSV and Sqlite have skeletons that need to be filled in. Adding a new
exporter is a matter of creating a new subclass of
`AbstractRouteVisitor` in `src/visitors` and referencing it in
`RouteVisitorFactory`. The visitor must have a `name()` class method
that is used while creating the argument parser.

### Body parts

Adding sensors on body parts simply requires extending the `BodyPart`
enumeration.

## Hardware

We experimented with an NGIMU from https://x-io.co.uk/.

## References
- https://www.nxp.com/docs/en/application-note/AN3397.pdf
- https://ieeexplore.ieee.org/document/7219743
- https://x-io.co.uk/open-source-imu-and-ahrs-algorithms/
- https://hal.inria.fr/hal-00966200/document
- https://www.researchgate.net/post/How_do_we_determine_the_position_of_an_object_with_accelerometer_and_gyroscope_sensor_data
- https://www.researchgate.net/post/How_can_I_calculate_displacement_from_accelerometer_data
- http://www.chrobotics.com/library/accel-position-velocity
- https://pykalman.github.io/
