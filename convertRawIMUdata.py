# -*- coding: utf-8 -*-

import csv
import time

inputFile = "sampleData/prise1.log"
outputFile = "prise1.csv"
dataLine = "/sensors"

rawDataFile  = open(inputFile, "r")
row = rawDataFile.readlines()

outputCSVfile = open(outputFile, 'w', newline='')
outputWriter = csv.writer(outputCSVfile, delimiter=';',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
outputWriter.writerow(['Timestamp', 'GyroX', 'GyroY', 'GyroZ', 'AccX', 'AvvY', 'AccZ', 'MagX', 'MagY', 'MagZ', 'Pressure'])

for line in row:
	words=line.split()
	if len(words)==11 and words[0] == dataLine :
		outputWriter.writerow([str(time.time()), words[1], words[2], words[3], words[4], words[5], words[6], words[7], words[8], words[9], words[10]])
	
